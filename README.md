# dof-exam

Baseline stuff for the SoftUni DevOps Fundamentals Course Exam (2019)

**Prerequisites:**

- Need to install keybase from keybase.io to be able to decrypt the secret key for the IAM user necessary.

- Need to have a domain. .tk are free (check: http://my.freenom.com)

- Need to have the kops utility installed

- need to have docker installed to build images

**Bringing up the infrastructure:**

Consult in that order the Readme files in:

terraform
kops
docker/images
kubernetes
