provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "softuni-dof-exam-tf-state"
    key    = "prerequisites/dns/terraform.tfstate"
    region = "eu-west-1"
  }
}

data "terraform_remote_state" "tf-state-remote-state" {
  backend = "s3"
  config {
    bucket = "softuni-dof-exam-tf-state"
    key    = "terraform.tfstate"
    region = "${var.region}"
  }
}
