variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-1"
}

variable "vpc-name" {
  description = "The name of the custom VPC"
  default = "dof-exam-vpc"
}

variable "public-hz-name" {
  description = "The name of the public hosted zone in Route53"
  default = "bzh-dof-exam.tk"
}

