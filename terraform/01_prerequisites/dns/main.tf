resource "aws_route53_zone" "primary" {
  name = "${var.public-hz-name}"

  tags {
    Name = "Primary Hosted Zone"
  }
}

resource "aws_route53_zone" "kops" {
  name = "kops.${var.public-hz-name}"

  tags = {
    Name = "Subdomain Hosted Zone for kops"
  }
}

resource "aws_route53_record" "kops-ns" {
  zone_id = "${aws_route53_zone.primary.zone_id}"
  name    = "${aws_route53_zone.kops.name}"
  type    = "NS"
  ttl     = "30"

  records = [
    "${aws_route53_zone.kops.name_servers.0}",
    "${aws_route53_zone.kops.name_servers.1}",
    "${aws_route53_zone.kops.name_servers.2}",
    "${aws_route53_zone.kops.name_servers.3}",
  ]
}
