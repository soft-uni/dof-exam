output "dns-servers" {
  value = "${aws_route53_zone.kops.name_servers}"
}
