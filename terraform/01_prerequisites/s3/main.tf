module "kops-state" {
  source      = "../../modules/s3"
  bucket-name = "${var.bucket-name}"
  region = "us-east-1"
  purpose = "S3 Bucket to store the kops state"
}
