output "kops-state-bucket" {
  value = "${module.kops-state.aws_s3_bucket}"
}
