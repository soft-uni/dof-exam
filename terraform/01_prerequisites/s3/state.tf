provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "softuni-dof-exam-tf-state"
    key    = "prerequisites/s3/terraform.tfstate"
    region = "eu-west-1"
  }
}

