variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-1"
}

variable "bucket-name" {
  description = "Name for the S3 bucket for kops state"
  default     = "softuni-dof-exam-kops-state"
}
