output "kops_user_access_key" {
  value = "${aws_iam_access_key.kops-user-access-key.id}"
}

output "kops_user_encrypted_secret_key" {
  value = "${aws_iam_access_key.kops-user-access-key.encrypted_secret}"
}

output "reminder" {
  value = <<REMINDER
############################################
WARNING:
When creating IAM users keybase.io PGP keys are used.
Therefore to get the created user's secret key use this command:
terraform output kops_user_encrypted_secret_key | base64 --decode | keybase pgp decrypt
############################################
REMINDER
}
