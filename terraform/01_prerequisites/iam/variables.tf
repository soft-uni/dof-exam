variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-1"
}

variable "kops-user" {
  description = "IAM user needed for the exam"
  default = "kops-user"
}
