resource "aws_iam_user" "kops-user" {
  name = "${var.kops-user}"
}

resource "aws_iam_access_key" "kops-user-access-key" {
  user = "${aws_iam_user.kops-user.name}"
  pgp_key = "keybase:dofexamuser"
}

resource "aws_iam_user_policy" "kops-user-policy" {
  user        = "${aws_iam_user.kops-user.name}"
  name        = "kops-user-iam-policy"

  policy = "${data.template_file.kops-user-policy.rendered}"
}
