resource "aws_vpc" "dof-exam-vpc" {
  cidr_block = "${var.vpc-cidr}"
  enable_dns_hostnames = true
  tags = {
      Name        = "${var.vpc-name}"
      Deployment   = "terraform"
      Purpose     = "Custom VPC for the DOF Exam"
  }
}

resource "aws_internet_gateway" "dof-exam-vpc-inet-gw" {
  vpc_id = "${aws_vpc.dof-exam-vpc.id}"

  tags = {
    Name        = "${var.vpc-name}-inernet-gateway"
    Deployment   = "terraform"
    Purpose     = "Internet Gateway for the custom VPC for the DOF Exam"
  }
}
