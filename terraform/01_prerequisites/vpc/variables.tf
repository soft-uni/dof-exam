variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-1"
}

variable "vpc-name" {
  description = "The name of the custom VPC"
  default = "dof-exam-vpc"
}

variable "vpc-cidr" {
  description = "The Range of the VPC"
  default = "10.0.0.0/16"
}

