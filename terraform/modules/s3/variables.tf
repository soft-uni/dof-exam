variable "bucket-name" {
  type        = "string"
  description = "The name of the bucket"
}

variable "purpose" {
  description = "Why is this resource created?"
}

variable "region" {
  description = "Which region to create the bucket"
}
