resource "aws_s3_bucket" "s3" {
  bucket = "${var.bucket-name}"
  acl    = "private"
  region = "${var.region}"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    prefix  = "/"
    enabled = true

    noncurrent_version_transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = 60
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days = 90
    }
  }

  tags {
    Name        = "${var.bucket-name}"
    Deployment   = "terraform"
    Purpose     = "${var.purpose}"
  }

  lifecycle {
    prevent_destroy = true
  }
}
