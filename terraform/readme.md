#Usage

**TERRAFORM STATE**

1. Go to 00_state and set your variables accordingly (region and bucket name)

2. Make sure only the provider part is uncommented in state.tf

3. Assume admin role for your AWS account and do "terraform init/apply"

4. After the bucket is created, uncomment the rest of the code in state.tf

5. Perform "terraform init/apply" again.

6. Confirm when prompted to migrate the state to the bucket

**PREREQUISITES:**

**IAM:**

1. Go to 01_prerequisites/iam and apply with terraform to create the kops user needed for the exam

2. In the same terminal execute:

```bash
echo "[default]" >> credentials
echo "aws_access_key_id = $(terraform output kops_user_access_key)" >> credentials
echo "aws_secret_access_key = $(terraform output kops_user_encrypted_secret_key | base64 --decode | keybase pgp decrypt)" >> credentials
cp ~/.aws/credentials ~/.aws/credentials_backup
cp credentials ~/.aws/credentials
```

**VPC:**

1. Go to 01_prerequisites/vpc and get the vpc module by "terraform get"

2. terraform apply

3. Write down the id-s of the private and public subnets and the vpc

**DNS:**

1. Go to 01_prerequisites/dns and apply with terraform

2. Write down the dns servers for the created hosted zone and set them as the nameservers for your domain (from its management panel)

**kops State:**

1. Go to 01_prerequisites/s3 and apply with terraform




