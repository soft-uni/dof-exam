provider "aws" {
  region = "${var.region}"
}

### uncomment this after you create the bucket and run terraform init again
terraform {
  backend "s3" {
    bucket = "softuni-dof-exam-tf-state"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

