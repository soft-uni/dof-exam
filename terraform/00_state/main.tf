module "s3-tf-state" {
  source      = "../modules/s3"
  bucket-name = "${var.bucket-name}"
  region = "${var.region}"
  purpose = "S3 Bucket to store the various terraform states"
}
