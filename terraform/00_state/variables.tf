variable "region" {
  description = "Region in which this is created"
  default     = "eu-west-1"
}

variable "bucket-name" {
  description = "Name for the S3 bucket for terraform states"
  default     = "softuni-dof-exam-tf-state"
}
