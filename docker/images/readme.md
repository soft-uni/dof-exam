Go to the "elk" folder and execute:

```bash
docker build -t registry.gitlab.com/soft-uni/dof-exam/elasticsearch:6.5.4 -f ./elasticsearch
docker build -t registry.gitlab.com/soft-uni/dof-exam/filebeat:6.5.4 ./filebeat
docker build -t registry.gitlab.com/soft-uni/dof-exam/kibana:6.5.4 ./kibana
docker build -t registry.gitlab.com/soft-uni/dof-exam/logstash:6.5.4 ./logstash
```

Push the images to a registry of your choice.

```bash
docker login registry.gitlab.com/soft-uni/dof-exam
docker push registry.gitlab.com/soft-uni/dof-exam/elasticsearch:6.5.4
docker push registry.gitlab.com/soft-uni/dof-exam/filebeat:6.5.4
docker push registry.gitlab.com/soft-uni/dof-exam/kibana:6.5.4
docker push registry.gitlab.com/soft-uni/dof-exam/logstash:6.5.4
```

Go to the exam folder and execute:

```bash
docker build -t registry.gitlab.com/soft-uni/dof-exam/nginx:latest -f ./Dockerfile-nginx .
docker build -t registry.gitlab.com/soft-uni/dof-exam/php:latest -f ./Dockerfile-php .
```

Push these images as well:

```bash
docker image list
docker push registry.gitlab.com/soft-uni/dof-exam/nginx
docker push registry.gitlab.com/soft-uni/dof-exam/php
```

