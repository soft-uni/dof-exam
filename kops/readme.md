# Create

Adjust the needed variables in cluster-config.yaml from what was built with terraform. For example, the state bucket, etc.

NB: Define the subnets. Public subnets should be of type Utility !

```yaml
/.../
metadata:
  name: <cluster-name>
/.../
  configBase: s3://<kops-state-bucket>
/.../  
  kubernetesVersion: <version>
/.../
  networkCIDR: <vpc-cidr>
  networkID: <vpc-id>
/.../
  subnets:
  - cidr: <private-subnet-1-cidr>
    name: <private-subnet-1-name>
    type: Private
    zone: <availability-zone-of-the-subnet>
/.../
  - cidr: <public-subnet-1-cidr>
    name: <public-subnet-1-name>
    type: Utility
    zone: <availability-zone-of-the-subnet>
 /.../
  topology:
/.../
    dns:
      type: Public
    masters: private
    nodes: private

---

apiVersion: kops/v1alpha2
kind: InstanceGroup
/.../
  image: kope.io/k8s-1.6-debian-jessie-amd64-hvm-ebs-2017-05-02
  machineType: t2.micro
  maxSize: 3
  minSize: 3
/.../
  subnets:
  - <private-subnet-1-name>
  - <private-subnet-2-name>
/.../
```

Set the needed variables in variables.sh and source it. 

Create the cluster:

```bash
kops create -f $NAME.yaml
bash setup_kops_ssh_key.sh <path/to/your/public/ssh/key>
kops update cluster $NAME --yes
kops rolling-update cluster $NAME --yes
```

Update the cluster spec YAML file, and to update the cluster run:

```bash
kops replace -f $NAME.yaml
kops update cluster $NAME --yes
kops rolling-update cluster $NAME --yes
```

#Destroy

1. Make sure you have the variables.sh file sourced.

2. Execute the below command:

```bash
kops delete cluster $NAME --yes
```
