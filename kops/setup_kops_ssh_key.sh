#!/usr/bin/env bash

# When you issue kops create cluster it will complain that you will have to add ssh key as kops secret.
# Source this file with first argument - the path to your key
kops create secret --name $NAME sshpublickey admin -i $1


