#!/usr/bin/env bash

# export NAME=<desired cluster name>.<your-registered-domain>
# EXAMPLE:
export NAME=kops.bzh-dof-exam.tk

# export KOPS_STATE_STORE=<the-url-of-your-kops-state-bucket>
# EXAMPLE:
export KOPS_STATE_STORE=s3://softuni-dof-exam-kops-state
