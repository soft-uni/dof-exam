#!/usr/bin/env bash

apt-get install git
git clone https://registry.gitlab.com/soft-uni/dof-exam.git

docker build -t registry.gitlab.com/soft-uni/dof-exam/nginx:latest -f ./Dockerfile-nginx .
docker build -t registry.gitlab.com/soft-uni/dof-exam/php:latest -f ./Dockerfile-php .
docker push registry.gitlab.com/soft-uni/dof-exam/nginx
docker push registry.gitlab.com/soft-uni/dof-exam/php
