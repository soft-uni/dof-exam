Make sure you are in the "kubernetes" folder

Create the needed namespaces:

```bash
kubectl get ns
kubectl create namespace elk
kubectl create namespace exam
```

Add the external-dns addon to map k8s services to DNS entries:

```bash
kubectl apply -f external-dns/external-dns.yaml
```

Modify the contents on each files in the folders as per your needs (which image to use, what namespace, etc.)

Execute:

```bash
kubectl create -f elastic-stack/ -R
kubectl create -f filebeat/ -R 
kubectl create -f kube-state-metrics/ -R
kubectl create -f metricbeat/ -R
```

Go to Kibana's DevTools and execute the PUT request below to RestAPI:

```json
PUT metricbeat-*/_mapping/doc
{
  "properties": {
    "kubernetes.pod.name": { 
      "type":     "text",
      "fielddata": true
    },
    "kubernetes.node.name": { 
      "type":     "text",
      "fielddata": true
    },
    "kubernetes.deployment.name": { 
      "type":     "text",
      "fielddata": true
    },
    "system.network.name": { 
      "type":     "text",
      "fielddata": true
    },   
    "system.filesystem.mount_point": { 
      "type":     "text",
      "fielddata": true
    },
    "system.process.name": { 
      "type":     "text",
      "fielddata": true
    },
    "beat.name": { 
      "type":     "text",
      "fielddata": true
    }
  }
}
```
Deploy the app:

```bash
kubectl create -f exam/ -R
```

Create the cronjob:

```bash
kubectl apply -f cronjob.yaml
```
